<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\SolarProject;
use App\Models\Contact;

class SolarProjectsTest extends TestCase
{
    public function testIndex()
    {
        $response = $this->get('/api/solar_projects');

        $response->assertStatus(200)
            ->assertJson([
                'links' => [
                    'next' => url('/api/solar_projects') . '?page=2',
                    'prev' => null,
                ],
            ]);
    }

    public function testShow()
    {
        $id = SolarProject::first()->uuid;

        $response = $this->json('GET', "/api/solar_projects/$id");

        $response->assertStatus(200)
            ->assertJson([
                'data' => [
                    'type' => 'solar_projects',
                    'id' => $id,
                ],
            ]);
    }

    public function testIncompletePut()
    {
        $id = SolarProject::first()->uuid;

        $newTitle = 'wont work';
        $response = $this->json('PUT', "/api/solar_projects/$id", [
            'title' => $newTitle,
        ]);

        $response->assertStatus(422)
            ->assertJson([
                'title' => 'Validation Exception',
                'errors' => [
                    'system_size' => ['The system size field must be present.'],
                    'site_latitude' => ['The site latitude field is required.'],
                    'site_longitude' => ['The site longitude field is required.'],
                ],
            ]);
    }

    public function testPatch()
    {
        $id = SolarProject::first()->uuid;

        $newTitle = 'test title ' . now()->toAtomString();
        $response = $this->json('PATCH', "/api/solar_projects/$id", [
            'title' => $newTitle,
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'data' => [
                    'attributes' => [
                        'title' => $newTitle,
                    ],
                ],
            ]);
    }

    public function testTask1()
    {

        # Update first contact with dummy firstname, lastname and email

        # Assert that updated contact attributes match dummy variables

        # Before completing Task 1, should fail as lastname = "Doe" would 
        # be found in data of returned contact

        $id = Contact::first()->uuid;

        $firstname = 'John';
        $lastname = 'Doe';
        $email = 'johndoe@email.com';

        $response = $this->json('PUT', "/api/contacts/$id", [
            'first_name' => $firstname,
            'last_name' => $lastname,
            'email' => $email
        ]);

        $response->assertStatus(200)
            ->assertJsonFragment([
                    'first_name' => $firstname,
                    'last_name' => $lastname,
                    'email' => $email
            ]);
    }

}
